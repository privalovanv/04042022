package utils;


import com.fasterxml.jackson.databind.ObjectMapper;
import selenide.mainPages.TestWords;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

public class Utils {
    private final static Properties prop = new Properties();
    private final static ObjectMapper objectMapper = new ObjectMapper();
    private static HashMap languages = new HashMap();
    private static HashMap header = new HashMap();

    public static TestWords testWords;

   static {
        try {
            initMappers();
            prop.load(Utils.class.getClassLoader().getResourceAsStream("setting.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void initMappers() throws IOException {
        File jsonFile = new File("src/main/resources/testWords.json");
        testWords = objectMapper.readValue(jsonFile, TestWords.class); // json to pojo

        File languagesFile = new File("src/main/resources/languages.json");
        languages = objectMapper.readValue(languagesFile, HashMap.class);

        File headerFile = new File("src/main/resources/header.json");
        header = objectMapper.readValue(headerFile, HashMap.class);
    }


    /**
     * Метод который читает файл languages.json в качестве HahsMap и достает значение по ключу
     * @param key ключ карты
     * @return значение карты
     */
    public static String getLanguage(String key){
       return (String) languages.get(key);
    }

    public static String getHeader(String key){
        return (String) header.get(key);
    }

    /**
     * Метод, который использует экземпляр Properties и читает из него значение по ключу из файла settings.properties
     * Через этот метод нужно получать все данные из файла в тестовом классе
     *
     * @param key название ключа. Например, указываем url - возвращается https://wikipedia.org/
     * @return значение ключа
     */
    public static String getProperty(String key) {
        return prop.getProperty(key);
    }

}
