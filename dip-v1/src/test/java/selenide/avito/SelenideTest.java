package selenide.Avito;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.codeborne.selenide.Selenide.*;


public class SelenideTest {

    @Test
    public void avito() throws InterruptedException {
        open("https://www.avito.ru/");
        open("https://www.wikipedia.org/");
        open("https://www.avito.ru/");
        SearchForm searchForm = new SearchForm();
        SearchPage searchPage = searchForm.search("Canon");
        Thread.sleep(5000);
        executeJavaScript("window.stop();");
        List<String> titles = searchPage.getHeaders();
        ElementsCollection prices = $$x("//span[@class= 'price-text-_YGDY text-text-LurtD text-size-s-BxGpL']");
        System.out.println("Общее количество ценников: " + prices.size());
        for (SelenideElement price : prices) {
            int value = Integer.parseInt(price.getText().replace('₽', ' ').replaceAll("\\s",""));
            System.out.println("Цена: " + value);
            Assertions.assertTrue(value < 200000);
        }
        closeWebDriver();
    }

}
