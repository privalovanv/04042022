package selenide.Avito;

import com.codeborne.selenide.ElementsCollection;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$$x;

public class SearchPage {
    private ElementsCollection headers = $$x("//h3");

    public List<String> getHeaders() {
        List<String> avitoHeaders = new ArrayList<>();
        headers.forEach(x -> avitoHeaders.add(x.getText()));
        return avitoHeaders;
    }
}
