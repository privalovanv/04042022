package selenide.Avito;
import org.openqa.selenium.By;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import static com.codeborne.selenide.Selenide.$;

public class SearchForm {
    private SelenideElement searchField = $(By.xpath("//input[@type=\"text\"]"));

    public SearchPage search(String str) {
        searchField.sendKeys(str, Keys.ENTER);
        return new SearchPage();
    }

}
