package selenide;

import com.codeborne.selenide.*;
import com.codeborne.selenide.logevents.SelenideLogger;

import io.qameta.allure.Allure;
import io.qameta.allure.selenide.AllureSelenide;

import org.junit.jupiter.api.*;
import org.openqa.selenium.OutputType;

import java.util.*;



public class TestHelperSelenide {
    private final Random random = new Random();


    @BeforeEach
    public void setUp() {
        Configuration.timeout = 10000;
        Configuration.pageLoadTimeout = 50000;
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide());
    }

    @AfterEach
    public void tearDown() {
        Double randomValue = random.nextDouble();
        Selenide.screenshot(String.valueOf(randomValue));
        String screenshotAsBase64 = Selenide.screenshot(OutputType.BASE64);
        byte[] decoded1 = Base64.getDecoder().decode(screenshotAsBase64);
        Allure.getLifecycle().addAttachment(String.valueOf(randomValue), "image/png", "png", decoded1);

        Selenide.clearBrowserCookies();
        Selenide.closeWebDriver();
    }

}
